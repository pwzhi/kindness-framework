<?php

use Sunshine\Console\Commands\InstallCommon;
use Sunshine\Console\Commands\ModuleCreateCommand;
use Sunshine\Console\Commands\ModuleListCommand;
use Sunshine\Console\Commands\ModuleMakeController;
use Sunshine\Console\Commands\ModuleMakeMiddleware;
use Sunshine\Console\Commands\ModuleMakeModel;
use Sunshine\Console\Commands\ModuleStart;
use Sunshine\Console\Commands\ModuleStop;

return [
    ModuleListCommand::class,
    ModuleCreateCommand::class,
    ModuleMakeController::class,
    ModuleMakeModel::class,
    ModuleMakeMiddleware::class,
    ModuleStart::class,
    ModuleStop::class,
    InstallCommon::class
];
